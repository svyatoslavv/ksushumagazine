// подключение Axios Для доступ через $axios во всем проекте
export default ({ $axios }) => {
  $axios.onError(err => {
    console.log(
      `[${err.response && err.response.status}] ${err.response &&
      err.response.request.path}`
    )
  })
}
