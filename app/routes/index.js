const ksushuRoutes = require('./ksushu_routes');
module.exports = function(app, db) {
    ksushuRoutes(app, db);
    // Тут, позже, будут и другие обработчики маршрутов
};