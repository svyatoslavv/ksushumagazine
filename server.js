// server.js
const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const app = express();
const cookieParser = require('cookie-parser');
const cors = require('cors')
const db = require('./config/db');

const port = 8000;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
MongoClient.connect(db.url, (err, client) => {
  if (err) console.log(err)
  require('./app/routes')(app, client);
  app.listen(port, () => {
    console.log('Back-end live on ' + port);
  });
})
