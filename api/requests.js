import Base from './_base'

export default class Auto extends Base {
  /** создание пользователя */
  createUser({ data }) {
    return this.$axios.post('create_user/', data)
  }
  /** авторизация пользователя */
  auth({ data }) {
    return this.$axios.post('auth/', data)
  }

  /** созданием item */
  createItem({ data }) {
    return this.$axios.post('create_item/', data)
  }

  /** получение item по id */
  getItem({ id }) {
    return this.$axios.get(`get_item/${id}`)
  }

  /** удаление item по id */
  deleteItem({ id }) {
    return this.$axios.delete(`delete_item/${id}`)
  }

  /** редактирование item по id */
  editItem({ id, data }) {
    return this.$axios.put(`edit_item/${id}`, data)
  }

  /** получение item по id */
  getItems() {
    return this.$axios.get('get_item/')
  }
}
