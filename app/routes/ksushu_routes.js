const ObjectID = require('mongodb').ObjectID

module.exports = function(app, client) {
    // POST создание пользователя
    app.post('/api/create_user', (req, res) => {
        const user = {
          email: req.body.email,
          name: req.body.name,
          password: req.body.password
        };
        const db = client.db('users');
        // проверка что с такой почтой уже существует пользователь
        const details = { 'email': req.body.email }
        db.collection("users").findOne(details, (err, item) => {
            if (err) {
                throw err
            } else if (item) {
                const response = {
                    success: false,
                    errors: 'Пользователь с такой эл. почтой уже существует'
                }
                res.send(response)
            } else {
                // создание пользователя
                db.collection("items").insert(user, (err, result) => {
                    if (err) {
                        throw err
                    } else {
                        const response = {
                            success: true
                        }
                        res.send(response)
                    }
                })
            }
        })
    })

    // POST создание товара
    app.post('/api/create_item', (req, res) => {
      const produce = {
          name: req.body.name,
          description: req.body.description,
          price: req.body.price,
          item_photo: req.body.item_photo,
          cloth_photo: req.body.cloth_photo

        };
        const db = client.db('items');
        // проверка что такого товара нету
        const details = { 'name': req.body.name };
        db.collection("items").findOne(details, (err, item) => {
            if (err) {
                throw err
            } else if (item) {
                const response = {
                    success: false,
                    error: 'Такой товар уже существует'
                }
                res.send(response)
            } else {
                // создание пользователя
                db.collection("items").insert(produce, (err, result) => {
                    if (err) {
                        throw err
                    } else {
                        const response = {
                            success: true,
                            result: result.ops[0]._id
                        };
                        res.send(response)
                    }
                })
            }
        })
    })

    // PUT редактирование товара
    app.put('/api/edit_item/:id', (req, res) => {
      const produce = {
          name: req.body.name,
          description: req.body.description,
          price: req.body.price,
          item_photo: req.body.item_photo,
          cloth_photo: req.body.cloth_photo

        };
        const db = client.db('items');
        // поиск по id
        const details = { '_id': ObjectID(req.params.id) };
      db.collection("items").updateOne(details, { $set: produce }, (err, result) => {
        if (err) {
          console.log(err)
          return res.sendStatus(500)
        }

        const response = {
          success: true
        }
        res.send(response)
      })
    })

    // DELETE удаление товара
    app.delete('/api/delete_item/:id', (req, res) => {
      const db = client.db('items');
      // поиск по id
      const details = { '_id': ObjectID(req.params.id) };
      db.collection("items").deleteOne(details, (err, result) => {
        if (err) {
          console.log(err)
          return res.sendStatus(500)
        }

        const response = {
          success: true
        }
        res.send(response)
      })
    })

    // GET получение товарoв
    app.get('/api/get_item', (req, res) => {
        const db = client.db('items');
        db.collection("items").find().toArray(function (err, docs) {
            if (err) {
                console.log(err)
                res.sendStatus(500)
            }
            let newArr = docs.slice()
            const result = {
                success: true,
                results: newArr.filter(doc => typeof doc.item_photo !== 'String')
            }
            res.send(result)
        })
    })

    // GET получение товара по id
    app.get('/api/get_item/:id', (req, res) => {
        const db = client.db('items');
        db.collection("items").findOne({ _id: ObjectID(req.params.id) }, function (err, doc) {
            if (err) {
                console.log(err)
                res.sendStatus(500)
            }
            const result = {
                success: true,
                result: doc
            }
            res.send(result)
        })
    });

    // авторизация
    app.post('/api/auth', (req, res) => {
      const db = client.db('users');
      const email = req.body.email
      const password = req.body.password
      db.collection('users').findOne({ email: email }, function (err, doc) {
        if (err) {
          res.sendStatus(500)
        }
        if (!doc) {
          const result = {
            success: false,
            error: 'Эл. почта не найдена'
          }
          res.send(result)
        } else if (doc.password !== password) {
          const result = {
            success: false,
            error: 'Неверный пароль'
          }
          res.send(result)
        } else res.send({
          success: true,
          result: { id: doc._id }
        })
      })
    })
};
