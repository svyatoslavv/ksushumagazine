// подключение апи для доступа через $api во всем проекте
import Api from '~/api'

export default function (ctx, inject) {
  const api =  new Api(ctx.$axios)

  ctx.$api = api

  inject('api', api)
}
