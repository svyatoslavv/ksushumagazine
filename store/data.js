export const state = () => ({
  banners: [
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner1.jpg',
      size: 'xs4',
      class: 'mr-4'
    },
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner2.jpg',
      size: 'xs4',
      class: 'mr-4 ml-4'
    },
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner3.jpg',
      size: 'xs4',
      class: 'ml-4'
    },
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner4.jpg',
      size: 'xs12',
      class: ''
    },
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner5.jpg',
      size: 'xs6',
      class: 'mr-4'
    },
    {
      title: 'Большой выбор тканей',
      description: 'Вы можете выбрать любую ткань на ваш вкус',
      name:'banner6.jpg',
      size: 'xs6',
      class: 'ml-4'
    }
  ]
})
