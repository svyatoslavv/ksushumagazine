import Requests from './requests'

export default class Api {
  constructor(axios) {
    this.requests = new Requests(axios)
  }
}
